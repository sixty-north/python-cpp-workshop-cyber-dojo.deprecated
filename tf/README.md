# Terraform your own cyber-dojo on AWS

## Step 1 - Create the AMI for your instance

This step is only necessary if you don't have an image already or if you need to
update the image. You would need to update the image if, for example, you change
the workshop exercises.

    packer build -var 'aws_secret_key=<secret key>' -var 'aws_access_key=<access key>' packer.json

Note that you might want to remove old AMIs when you do this.

## Step 2 - Get your aws keys

Download your private key and put it in a folder ssh/mykey.pem

## Step 3 - create a `terraform.tfvars` file

Copy `terraform.tfvars.TEMPLATE` to `terraform.tfvars` and edit as appropriate.

## Step 4 - run the terraform

    terraform plan
    terraform apply

At this point the cyber dojo should be up and running with the Boost.Python
workshop available. You still need to manually update the DNS on mythic-beasts
if you want cyberdojo.sixty-north.com to work.

## Step 5 - destroy your server

    terraform destroy
