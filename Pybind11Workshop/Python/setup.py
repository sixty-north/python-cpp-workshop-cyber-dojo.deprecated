# This will build the `bitmap` modules from bitmap.cpp and install it.

from setuptools import setup, Extension  # Always prefer setuptools over distutils

ext_module = Extension(
    'bitmap',
    sources=['bitmap.cpp'],
    include_dirs=['.', '/usr/include/', '/workshop/include'],
    library_dirs=[],
    libraries=[],
    extra_compile_args=['--save-temps'])

setup(
    name='bitmap',
    description='Pybind11 module for working with BMP files',

    # Author details
    author='Sixty North AS',
    author_email='austin@sixty-north.com',

    ext_modules=[ext_module]
)
