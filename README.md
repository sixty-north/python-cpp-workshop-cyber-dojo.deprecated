# Boost.Python Workshop Cyberdojo support

This is embeds our boost.python/pybind11 workshops into cyberdojo, making it
easier for students to do the workshop with no setup necessary on their
machines.

This involves a few parts:

1. A docker image in which the cyberdojo tests run.
2. A cyberdojo "start-point" definition.
3. Terraform scripts for setting up the cyberdojo on AWS.

## Docker image

This image contains all of the libraries and headers that are needed for
compiling and linking the exercises in the workshop. The definition of the image
is in the `docker` directory. The latest version of the image is available on
dockerhub as `sixtynorth/boost-python-workshop`.

If you need to update the image, just modify the `Dockerfile` as needed and run
`docker/upload-image.sh`. This should do the right thing.

## Cyberdojo start-point

This is essentially the exercises and test infrastructure that students will
interact with in the workshop. It's embodied in the `BoostPythonWorkshop` and
`Pybind11Workshop` directories. This will need to be updated as we update the
workshop. If you change the start point, you'll need to restart any instances
that are already running the dojo (assuming you want those to have the update).
No other changes should be necessary.

## Terraform

The terraform setup is kept in the `tf` directory. It's sufficiently complex
that it has its own README.
