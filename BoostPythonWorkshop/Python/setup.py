# This will build the `bitmap` modules from bitmap.cpp and install it.

from setuptools import setup, Extension  # Always prefer setuptools over distutils

ext_module = Extension(
    'bitmap',
    sources=['bitmap.cpp'],
    include_dirs=['.', '/usr/include/'],
    library_dirs=['/usr/lib/x86_64-linux-gnu'],
    libraries=['boost_python-py35'])

setup(
    name='bitmap',
    description='Boost.Python module for working with BMP files',

    # Author details
    author='Sixty North AS',
    author_email='austin@sixty-north.com',

    ext_modules=[ext_module]
)
