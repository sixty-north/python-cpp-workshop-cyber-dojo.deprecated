# Modify this to reflect the exercise you're currently working on.
EXERCISE=1a

# DO NOT EDIT BELOW HERE
export PYTHONPATH=$PYTHONPATH:build/lib.linux-x86_64-3.5
python3 setup.py build
pytest test.py --ex $EXERCISE
